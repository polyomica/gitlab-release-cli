package testdata

import (
	"io/ioutil"
	"net/http"
	"strings"
)

const (
	ResponseCreateReleaseSuccess = iota
	ResponseBadRequest
	ResponseUnauthorized
	ResponseForbidden
	ResponseConflict
	ResponseInternalError
	ResponseUnexpectedError
	ResponseMilestoneNotFound
	ResponseGetReleaseSuccess
)

// Responses map of API possible responses obtained from the Releases API
var Responses = map[int]func() *http.Response{
	ResponseCreateReleaseSuccess: func() *http.Response {
		return &http.Response{
			StatusCode: http.StatusCreated,
			Body:       ioutil.NopCloser(strings.NewReader(CreateReleaseSuccessResponse)),
		}
	},
	ResponseBadRequest: func() *http.Response {
		return &http.Response{
			StatusCode: http.StatusBadRequest,
			Body:       ioutil.NopCloser(strings.NewReader(BadRequestResponse)),
		}
	},
	ResponseUnauthorized: func() *http.Response {
		return &http.Response{
			StatusCode: http.StatusUnauthorized,
			Body:       ioutil.NopCloser(strings.NewReader(UnauthorizedResponse)),
		}
	},
	ResponseForbidden: func() *http.Response {
		return &http.Response{
			StatusCode: http.StatusForbidden,
			Body:       ioutil.NopCloser(strings.NewReader(CreateReleaseForbiddenResponse)),
		}
	},
	ResponseConflict: func() *http.Response {
		return &http.Response{
			StatusCode: http.StatusConflict,
			Body:       ioutil.NopCloser(strings.NewReader(CreateReleaseConflictResponse)),
		}
	},
	ResponseInternalError: func() *http.Response {
		return &http.Response{
			StatusCode: http.StatusInternalServerError,
			Body:       ioutil.NopCloser(strings.NewReader(InternalErrorResponse)),
		}
	},
	ResponseUnexpectedError: func() *http.Response {
		return &http.Response{
			StatusCode: http.StatusInternalServerError,
			Body:       ioutil.NopCloser(strings.NewReader(UnexpectedErrorResponse)),
		}
	},
	ResponseMilestoneNotFound: func() *http.Response {
		return &http.Response{
			StatusCode: http.StatusBadRequest,
			Body:       ioutil.NopCloser(strings.NewReader(CreateReleaseMilestoneNotFound)),
		}
	},
	ResponseGetReleaseSuccess: func() *http.Response {
		return &http.Response{
			StatusCode: http.StatusOK,
			Body:       ioutil.NopCloser(strings.NewReader(GetReleaseResponseSuccess)),
		}
	},
}

const BadRequestResponse = `
{
	"message":"tag_name is missing"
}
`

const UnauthorizedResponse = `
{
	"message":"401 Unauthorized"
}
`

const InternalErrorResponse = `
{
	"message":"500 Internal Server Error"
}
`

const UnexpectedErrorResponse = `
{
	"error":"Something went wrong"
}
`
