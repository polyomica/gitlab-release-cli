#!/bin/bash

set -uo pipefail

base_path="$PWD/internal/testdata/certs"
gen_rsa_key () {
  echo "Generating RSA key: $1"
  rm -f "$1"
  openssl genrsa -out "$1" 2048
}

# openssl req -x509 -new -nodes -key CA.key -sha256 -out CA.pem
gen_ca_pem () {
  echo "Generating CA certificate: $2"
  rm -f "$2"
  openssl req -x509 -new -nodes -days 3650 -key "$1" -sha256 -out "$2" -config "$base_path/ca_openssl.conf"
}

# openssl req -new -key localhost.key -out localhost.csr
gen_csr () {
  echo "Generating certificate signing request (CSR) for: $2"
  rm -f "$2"
  openssl req -new -key "$1" -out "$2" -config "$PWD"/internal/testdata/certs/openssl.conf
}

# openssl x509 -req -in localhost.csr -CA CA.pem -CAkey CA.key -CAcreateserial -out localhost.pem -sha256 -extfile v3.ext
gen_self_signed_cert () {
  echo "Generating self-signed-certificate: $4"
  rm -f "$4"
  openssl x509 -req -days 3650 -in "$1" -CA "$2" -CAkey "$3" -CAcreateserial -out "$4" -sha256 -extfile "$base_path/v3.ext"
}


gen_all () {
  gen_rsa_key "$base_path/CA.key"
  gen_ca_pem "$base_path/CA.key" "$base_path/CA.pem"
  gen_rsa_key "$base_path/localhost.key"
  gen_csr "$base_path/localhost.key" "$base_path/localhost.csr"
  gen_self_signed_cert "$base_path/localhost.csr" "$base_path/CA.pem" "$base_path/CA.key" "$base_path/localhost.pem"
}
