// +build !windows

package commands

import (
	"io/ioutil"
	"testing"

	testlog "github.com/sirupsen/logrus/hooks/test"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/release-cli/internal/testhelpers"
)

func TestGetDescriptionSymlink(t *testing.T) {
	chdirSet := false
	chdir := testhelpers.ChdirInPath(t, "../", &chdirSet)

	defer chdir()

	logger, _ := testlog.NewNullLogger()

	file := "testdata/description.txt"
	content, err := ioutil.ReadFile(file)
	require.NoError(t, err)

	tests := []struct {
		name     string
		arg      string
		expected string
	}{
		{
			name:     "description_from_file_from_valid_symlink",
			arg:      "testdata/description-symlink-valid.txt",
			expected: string(content),
		},
		{
			name:     "description_from_file_from_malicious_symlink",
			arg:      "testdata/description-symlink-malicious.txt",
			expected: "testdata/description-symlink-malicious.txt",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			description, err := getDescription(tt.arg, logger)

			require.NoError(t, err)
			require.Equal(t, tt.expected, description)
		})
	}
}
